class V1::ArticlesController < ApiController

  def index 
    data = Article.joins(:user).select("articles.*, users.email")
    render json: {status: true, result: data, error: nil}, status: 200
  end

end