class ArticlesController < ApplicationController
  
  def create
    @article = Article.new(article_params)
    @article.user_id = current_user.id
    # current_user.article.new
 
    if @article.save
      redirect_to @article
    else
      render 'new'
    end
  end

  def show
    @article = Article.find(params[:id])
  end

  def edit
    @article = Article.find(params[:id])
    if @article.user_id != current_user.id
      flash[:notice] = "Unauthorized user"
      redirect_to articles_path
    end
  end

  def update
    @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to @article
    else
      render 'edit'
    end
  end

  def new
    @article = Article.new
  end

  def index
    @articles = Article.all
  end

  def myindex
    @articles = Article.where("user_id = #{current_user.id}")
  end

  def destroy
    @article = Article.find(params[:id])
    if @article.user_id == current_user.id
      @article.destroy
    else
      flash[:notice] = "Unauthorized user"
    end
    redirect_to articles_path
  end

  private 
  def article_params
    params.require(:article).permit(:title, :text, :image)
  end
end
