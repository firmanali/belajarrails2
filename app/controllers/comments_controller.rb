class CommentsController < ApplicationController
  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.create(comment_params)
    if current_user == nil
      @comment.commenter = "Anonymous"
    else
      @comment.commenter = User.find(current_user.id).fullname
      @comment.user_id = current_user.id
    end
    @comment.save
    redirect_to article_path(@article)
  end

  def destroy
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    @comment.destroy
    redirect_to article_path(@article)
  end
     
  private
    def comment_params
    params.require(:comment).permit(:body)
  end
end
