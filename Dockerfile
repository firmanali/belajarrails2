FROM ruby:2.5.6-stretch
RUN apt-get update && apt-get install -y build-essential libpq-dev yarn net-tools
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - \
    && apt install -y nodejs
RUN mkdir /application
WORKDIR /application
COPY . .
COPY Gemfile /application
COPY Gemfile.lock /application
RUN gem install bundler -v 2.0.2
RUN bundle install
ENV RAILS_ENV production
ENTRYPOINT ./entrypoint.sh
